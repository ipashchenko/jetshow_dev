class Ray(object):
    def __init__(self, origin, direction):
        self._origin = origin
        self._direction = direction

    def point(self, t):
        return self._origin + self._direction*t

    @property
    def origin(self):
        return self._origin

    @property
    def direction(self):
        return self._direction