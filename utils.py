from numba import jit
import numpy as np
from scipy.special import gamma as tgamma
from astropy import units as u
from astropy import constants as const

q_e = const.e.gauss.value
m_e = const.m_e.to(u.g).value
m_p = const.m_p.to(u.g).value
c = const.c.to(u.cm/u.s).value
to_jy = (u.erg/u.s/u.Hz/u.cm**2).to(u.Jansky)
pc = u.pc.to(u.cm)


@jit
def nu_p(n):
    """
    Plasma frequency.

    :param n:
        Concentration [cm^-3]
    """
    return np.sqrt(n * q_e * q_e / (np.pi * m_e))


@jit
def nu_b(b, n_los):
    """
    Larmor frequency.

    :param b:
        Vector of the magnetic field.
    :param n_los:
        Vector of LOS.
    """
    return q_e*np.linalg.norm(np.cross(n_los, b))/(2.*np.pi*m_e*c)


def nu_b_value(b):
    """
    Larmor frequency.

    :param b:
        Vector of the magnetic field.
    """
    return q_e*np.linalg.norm(b)/(2.*np.pi*m_e*c)


def nu_b_rand(b):
    """
    Larmor frequency.

    :param b:
        Value of the magnetic field.
    """
    return q_e*b/(2.*np.pi*m_e*c)


@jit
def k_0(b, n_los, nu, n):
    """
    Coefficient in expression for absorption coefficient for arbitrary B-field.
    :param b:
        Vector of the magnetic field.
    :param n_los:
    :param nu:
    :param n:
    """
    return np.pi*nu_p(n)*nu_p(n)*nu_b(b, n_los)/(c*nu*nu)


def k_0_value(b, nu, n):
    """
    :param b:
        Vector of the magnetic field.
    :param nu:
    :param n:
    """
    return np.pi*nu_p(n)*nu_p(n)*nu_b_value(b)/(c*nu*nu)


def k_0_rnd(b, nu, n):
    """
    Coefficient in expression for absorption coefficient for random B-field.
    :param b:
        Value of the magnetic field.
    :param nu:
    :param n:
    """
    return np.pi*nu_p(n)*nu_p(n)*nu_b_rand(b)/(c*nu*nu)


@jit
def k_i(b, n_los, nu, n, s=2.5):
    """
    Absorption coefficient for arbitrary B-field.
    :param b:
        Vector of the magnetic field.
    :param n_los:
    :param nu:
    :param n:
    :param s:
    """
    factor = (np.power(3., (s+1.)/2.)/4.)*tgamma(s/4.+11./6.)*tgamma(s/4.+1./6.)
    return k_0(b, n_los, nu, n)*np.power(nu_b(b, n_los)/nu, s/2.)*factor


def k_i_rnd(b, n_los, nu, n, s=2.5):
    """
    Absorption coefficient for random B-field.
    :param b:
        Vector of the magnetic field.
    :param n_los:
    :param nu:
    :param n:
    :param s:
    """
    factor = (np.power(3., (s+1.)/2.)/4.)*tgamma(s/4.+11./6.)*tgamma(s/4.+1./6.)
    rnd_factor = np.sqrt(np.pi/4.)*tgamma((6.+s)/4.)/tgamma((8.+s)/4.)
    factor = factor(rnd_factor)
    return k_0(b, n_los, nu, n)*np.power(nu_b_rand(b)/nu, s/2.)*factor


def k_q(b, n_los, nu, n, s=2.5):
    """
    :param b:
        Vector of the magnetic field.
    :param n_los:
    :param nu:
    :param n:
    :param s:
    """
    return (s+2.)/(s+10./3)*k_i(b, n_los, nu, n, s)


def k_u():
    return 0.0


@jit
def k_v(b, n_los, nu, n, s=2.5):
    """
    :param b:
        Vector of the magnetic field.
    :param n_los:
    :param nu:
    :param n:
    :param s:
    """
    cos_theta = b.dot(n_los)/np.linalg.norm(b)
    factor = np.power(3., s/2.)*(s+3.)*(s+2.)/(4.*(s+1.))*tgamma(s/4.+11./12.)*tgamma(s/4.+7./12.)
    return -k_0_value(b, nu, n)*cos_theta*np.power(nu_b(b, n_los)/nu, (s+1.)/2.)*factor


@jit
def k_F_c(b, n_los, nu, n):
    """
    :param b:
        Vector of the magnetic field.
    :param n_los:
    :param nu:
    :param n:
    :param s:
    """
    cos_theta = b.dot(n_los)/np.linalg.norm(b)
    return 2.*np.pi*nu_p(n)*nu_p(n)*nu_b_value(b)*cos_theta/(c*nu*nu)


@jit
def k_C_c(b, n_los, nu, n, s=2.5):
    """
    :param b:
        Vector of the magnetic field.
    :param n_los:
    :param nu:
    :param n:
    :param s:
    """
    return -np.pi*nu_p(n)*nu_p(n)*nu_b(b, n_los)*nu_b(b, n_los)/(c*nu*nu*nu)


@jit
def k_F_r(b, n_los, nu, n, gamma_min, s=2.5):
    """
    :param b:
        Vector of the magnetic field.
    :param n_los:
    :param nu:
    :param n:
    :param gamma_min:
    :param s:
    """
    return (s+2.)*np.log(gamma_min)/((s+1.)*np.power(gamma_min, s+1.))*k_F_c(b, n_los, nu, n)


@jit
def k_C_r(b, n_los, nu, n, gamma_min, s=2.5):
    """
    :param b:
        Vector of the magnetic field.
    :param n_los:
    :param nu:
    :param n:
    :param gamma_min:
    :param s:
    """
    return (2./(s-2.))*(np.power(gamma_min, 2.-s)-np.power(nu_b(b, n_los)/nu, (s-2.)/2.))*k_C_c(b, n_los, nu, n, s)


def h_Q():
    return 0


@jit
def eta_0(b, n_los, n):
    """
    Coefficient in expression for emission coefficient for arbitrary B-field.
    :param b:
        Vector of the magnetic field.
    :param n_los:
    :param n:
    """
    return np.pi*nu_p(n)**2*nu_b(b, n_los)*m_e/c


def eta_0_value(b, n):
    """
    :param b:
        Vector of the magnetic field.
    :param nu:
    :param n:
    """
    return np.pi*nu_p(n)*nu_p(n)*nu_b_value(b)*m_e/c


def eta_0_rnd(b, n_los, n):
    """
    Coefficient in expression for emission coefficient for random B-field.
    :param b:
        Value of the magnetic field.
    :param n_los:
    :param n:
    """
    return np.pi*nu_p(n)*nu_p(n)*nu_b_rand(b)*m_e/c


@jit
def eta_i(b, n_los, nu, n, s=2.5):
    """
    Emission coefficient for arbitrary B-field.
    :param b:
        Vector of the magnetic field.
    :param n_los:
    :param nu:
    :param n:
    :param s:
    """
    factor = np.power(3., s/2.)/(2.*(s+1))*tgamma(s/4.+19./12.)*tgamma(s/4.-1./12.)
    return eta_0(b, n_los, n)*np.power(nu_b(b, n_los)/nu, (s-1.)/2.)*factor


def eta_i_rnd(b, n_los, nu, n, s=2.5):
    """
    Emission coefficient for random B-field.
    :param b:
        Vector of the magnetic field.
    :param n_los:
    :param nu:
    :param n:
    :param s:
    """
    factor = np.power(3., s/2.)/(2.*(s+1))*tgamma(s/4.+19./12.)*tgamma(s/4.-1./12.)
    rnd_factor = np.sqrt(np.pi/4.)*tgamma((5.+s)/4.)/tgamma((7.+s)/4.)
    factor = factor*rnd_factor
    return eta_0(b, n_los, n)*np.power(nu_b_rand(b)/nu, (s-1.)/2.)*factor


def eta_q(b, n_los, nu, n, s=2.5):
    """
    :param b:
        Vector of the magnetic field.
    :param n_los:
    :param nu:
    :param n:
    :param s:
    """
    return (s+1.0)/(s+7./3.)*eta_i(b, n_los, nu, n, s)


def eta_u():
    return 0.0


@jit
def eta_v(b, n_los, nu, n, s=2.5):
    """
    :param b:
        Vector of the magnetic field.
    :param n_los:
    :param nu:
    :param n:
    :param s:
    """
    cos_theta = b.dot(n_los)/np.linalg.norm(b)
    factor = np.power(3., (s-1.)/2.)*(s+2.)/(2.*s)*tgamma(s/4.+2./3.)*tgamma(s/4.+1./3.)
    return -eta_0_value(b, n)*cos_theta*np.power(nu_b(b, n_los)/nu, s/2.)*factor


@jit
def getG(v):
    """
    Lorenz factor.
    :param v:
        Vector of velocity.
    """
    return 1/np.sqrt(1-np.linalg.norm(v/c)**2)


@jit
def getD(n_los, v):
    """
    Doppler factor for LOS unit vector ``n_los`` in lab frame and bulk velocity
    ``v`` relative to the lab frame.
    :param v:
        Vector of velocity.
    """
    return 1/(getG(v)*(1-(v/c).dot(n_los)))


@jit
def get_n_los_prime(n_los, v):
    """
    LOS unit vector in the comoving frame. This is relativistic aberration.
    :param n_los:
        LOS unit vector in lab frame.
    :param v:
        Velocity of comoving frame relative to lab frame.
    """
    df = getD(n_los, v)
    gamma = getG(v)
    beta = v/c
    return df*n_los-(df+1.)*(gamma/(gamma+1.))*beta


def convert_point_to_r_r_perp(point):
    return np.array([point[2], np.hypot(point[0], point[1])])


if __name__ == "__main__":
    print("q_e={}, m_e={}, c={}".format(q_e, m_e, c))