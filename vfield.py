import numpy as np
from abc import ABC, abstractmethod
from scipy.interpolate import LinearNDInterpolator, CloughTocher2DInterpolator
from utils import c, convert_point_to_r_r_perp


class VField(ABC):
    @abstractmethod
    def value(self, point):
        pass


class SimulationOutputVField(VField):
    def __init__(self, points, values):
        print("Inerpolating velocity field...")
        self.interpolator = CloughTocher2DInterpolator(points, values,
                                                       fill_value=0.0,
                                                       rescale=True)

    def value(self, point):
        gamma = self.interpolator(convert_point_to_r_r_perp(point))[0]
        if not gamma:
            return np.zeros(3, dtype=float)
        return np.array([0, 0, c*np.sqrt(1-1/(gamma*gamma))])


class ConstFlatVField(VField):
    def __init__(self, gamma):
        self.gamma = gamma

    def value(self, point):
        return np.array([0, 0, c*np.sqrt(1-1/(self.gamma*self.gamma))])


class SheathFlatVField(VField):
    def __init__(self, gamma_spine, gamma_sheath, r_sheath):
        self.gamma_spine = gamma_spine
        self.gamma_sheath = gamma_sheath
        self.r_sheath = r_sheath

    def value(self, point):
        r = np.hypot(point[0], point[1])
        if r < self.r_sheath:
            gamma = self.gamma_spine
        else:
            gamma = self.gamma_sheath
        return np.array([0, 0, c*np.sqrt(gamma**2-1)/gamma])


class ShearedFlatVField(VField):
    def __init__(self, gamma_axis, gamma_border, r_border):
        self.gamma_axis = gamma_axis
        self.gamma_border = gamma_border
        self.r_border = r_border

    def value(self, point):
        r = np.hypot(point[0], point[1])
        gamma = self.gamma_axis-(self.gamma_axis-self.gamma_border)*r/self.r_border
        return np.array([0, 0, c*np.sqrt(gamma**2-1)/gamma])


class ConstCentralVField(VField):
    def __init__(self, gamma):
        self.gamma = gamma

    def value(self, point):
        r = np.linalg.norm(point)
        v_r = c*np.sqrt(self.gamma**2-1)/self.gamma
        return np.array([v_r*point[0]/r, v_r*point[1]/r, v_r*point[2]/r])


class SheathCentralVField(VField):
    def __init__(self, gamma_spine, gamma_sheath, theta_sheath):
        self.gamma_spine = gamma_spine
        self.gamma_sheath = gamma_sheath
        self.theta_sheath = theta_sheath

    def value(self, point):
        r = np.linalg.norm(point)
        theta = np.arccos(point[2]/r)
        if theta < self.theta_sheath:
            gamma = self.gamma_spine
        else:
            gamma = self.gamma_sheath
        v_r = c*np.sqrt(gamma**2-1)/gamma
        return np.array([v_r*point[0]/r, v_r*point[1]/r, v_r*point[2]/r])


class ShearedCentralVField(VField):
    def __init__(self, gamma_axis, gamma_border, theta_border):
        self.gamma_axis = gamma_axis
        self.gamma_border = gamma_border
        self.theta_border = theta_border

    def value(self, point):
        r = np.linalg.norm(point)
        theta = np.arccos(point[2]/r)
        gamma = self.gamma_axis-(self.gamma_axis-self.gamma_border)*theta/self.theta_border
        v_r = c * np.sqrt(gamma ** 2 - 1) / gamma
        return np.array([v_r*point[0]/r, v_r*point[1]/r, v_r*point[2]/r])


if __name__ == "__main__":
    from utils import pc
    vfield = ConstFlatVField(10)
    point = np.array([0, 0, 5*pc])
    print("V at given r = {}".format(vfield.value(point)))