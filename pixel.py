class Pixel(object):
    def __init__(self, size, coordinate, ij):
        self.size = size
        self._coordinate = coordinate
        self.ij = ij
        self.values = dict()

    def get_value(self, value):
        try:
            return self.values[value]
        except KeyError:
            return 0.0

    def set_value(self, value, new_value):
        self.values[value] = new_value

    @property
    def coordinate(self):
        return self._coordinate