import numpy as np
from pixel import Pixel


class Image(object):
    def __init__(self, image_size, pixel_size, pixel_scale):
        self.image_size = image_size
        self.pixel_size = pixel_size
        self.pixel_scale = pixel_scale
        self.num_of_pixels = image_size[0]*image_size[1]
        self._pixels = list()
        for i in range(0, image_size[0]):
            for j in range(0, image_size[1]):
                coordinate = self.get_scaled_coordinate(i, j)
                self._pixels.append(Pixel(pixel_size, coordinate, (i, j,)))

    def get_coordinate(self, i, j):
        return np.array([0.0,
                         i-self.image_size[0]/2.+0.5,
                         j-self.image_size[1]/2.+0.5])

    def get_scaled_coordinate(self, i, j):
        return self.pixel_size*self.get_coordinate(i, j)

    def get_image(self, value):
        image = list()
        for i in range(0, self.image_size[0]):
            for j in range(0, self.image_size[1]):
                image.append(self.pixels[i*self.image_size[0] + j].get_value(value))

        return np.array(image).reshape(self.image_size)

    @property
    def pixels(self):
        return self._pixels
