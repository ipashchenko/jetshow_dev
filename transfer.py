import numpy as np
from scipy.integrate import RK45
from scipy.optimize import brentq
from utils import c, to_jy, get_n_los_prime, pc
from astropy import cosmology


class Transfer(object):
    _available_modes = ("I", "IQUV")
    cosmo = cosmology.WMAP9

    def __init__(self, jet, image_plane, nu, z):
        self.jet = jet
        self.image_plane = image_plane
        self.nu = nu*(1+z)
        self.z = z

    @property
    def pixel_solid_angle(self):
        return (self.image_plane.image.pixel_size/self.cosmo.angular_diameter_distance(self.z).to(u.pc).value)**2

    @property
    def scale(self):
        return self.pixel_solid_angle*to_jy/(1. + self.z)**3

    def run(self, lg_tau_max, lg_tau_min, mode="I"):
        if mode not in self._available_modes:
            raise Exception("Only I and IQUV modes are available!")
        if self.jet.randomB and mode != "I":
            raise Exception("Random B-field is used only with I mode!")
        rays = self.image_plane.rays
        pixels = self.image_plane.pixels
        image_size = self.image_plane.image_size
        for j in range(0, image_size[0]):
            for k in range(int(image_size[1]/2), image_size[1]):
                ray = rays[j*image_size[0]+k]
                pixel = pixels[j*image_size[0]+k]
                self.observe_single_pixel(ray, pixel, lg_tau_min, lg_tau_max,
                                          mode)

    def observe_single_pixel(self, ray, pixel, lg_tau_min, lg_tau_max, mode):
        print("Pixel # {}".format(pixel.ij))
        ray_direction = ray.direction
        list_intersect = self.jet.hit(ray)

        if not list_intersect:
            print("No intersection!")
        else:
            # print("List intersections : {}".format(list_intersect[0].borders))
            # If tau will exceed tau_max then change ``list_intersect``!
            list_intersect, tau, l = self.integrate_tau(list_intersect,
                                                        ray_direction,
                                                        self.nu,
                                                        lg_tau_max)
            # print(tau)

            if tau > 10 ** lg_tau_min:
                if mode == "I":
                    result = self.integrate_i(list_intersect,
                                              ray_direction,
                                              self.nu)
                elif mode == "IQUV":
                    result = self.integrate_full(list_intersect,
                                                 ray_direction,
                                                 self.nu)
            else:
                if mode == "I":
                    result = 0.0
                elif mode == "IQUV":
                    result = np.zeros(4, dtype=float)

            pixel.set_value("tau", tau)
            pixel.set_value("l", l)
            if mode == "I":
                pixel.set_value("I", result)
            elif mode == "IQUV":
                pixel.set_value("I", result[0])
                pixel.set_value("Q", result[1])
                pixel.set_value("U", result[2])
                pixel.set_value("V", result[3])

    def integrate_tau(self, list_intersect, ray_direction, nu, lg_tau_max):
        tau_max = 10**lg_tau_max
        ray_direction_ = -ray_direction

        def dtau_dt(t, y):
            return self.jet.getKI(point+t*ray_direction_, ray_direction_, nu)

        tau, t = 0, 0
        for i, it in enumerate(list_intersect):
            point_in, point_out = it.get_path()
            length = np.linalg.norm(point_in-point_out)
            point = point_in
            rk = RK45(dtau_dt, t, np.array([tau]), length, atol=1e-9, rtol=1e-9)
            while rk.status == "running":
                print("y_old = ", rk.y_old)
                rk.step()
                if rk.y_old < tau_max < rk.y:
                    do = rk.dense_output()
                    t_tau_max = brentq(lambda x: do(x)-tau_max, do.t_min, do.t_max)
                    point = point+t_tau_max*ray_direction_
                    it.set_point_out(point)
                    list_intersect = list_intersect[:i+1]
                    tau = do(t_tau_max)
                    t = t_tau_max
                    return list_intersect, tau, t
            tau += rk.y[0]
            t += length

        return list_intersect, tau, t

    def integrate_i(self, list_intersect, ray_direction, nu):
        ray_direction_ = -ray_direction

        def dI_dt(t, y):
            return self.jet.getEtaI(point+t*ray_direction_, ray_direction_, nu) -\
                   self.jet.getKI(point+t*ray_direction_, ray_direction_, nu)*y

        I, t = 0.0, 0.0
        for i, it in enumerate(list_intersect):
            point_in, point_out = it.get_path()
            length = np.linalg.norm(point_in-point_out)
            point = point_out
            rk = RK45(dI_dt, t, np.array([I]), length, atol=1e-9, rtol=1e-9)
            while rk.status == "running":
                rk.step()
                print(rk.t, rk.y)
                if rk.y[0] < 0:
                    rk.y[0] = 0.0
            I += rk.y[0]
            t += length
        return I*self.scale

    def chi(self, point, n_los, b_plasma_frame=True):
        # Jet axis
        l = np.array([0, 0, 1])
        # Projection of jet axis on the plane of the sky
        l_proj = l - np.dot(l, n_los)*n_los
        # Make it unit vector
        l_proj = l_proj/np.linalg.norm(l_proj)

        b_unit = self.jet.bfield.value(point)
        b_unit = b_unit / np.linalg.norm(b_unit)
        beta = self.jet.vfield.value(point)/c
        if b_plasma_frame:
            gamma = self.jet.getGamma(point)
            q = b_unit+np.cross(n_los, np.cross(beta, b_unit))-(gamma/(gamma+1))*np.dot(b_unit, beta)*beta
        else:
            q = b_unit+np.cross(n_los, np.cross(beta, b_unit))
        e = np.cross(n_los, q)/np.sqrt(np.linalg.norm(q)**2-np.dot(n_los, q)**2)

        # Projection of e_primed on a plane perpendicular to n_los
        e_proj = e - np.dot(e, n_los)*n_los
        # Make it unit vector
        e_proj = e_proj/np.linalg.norm(e_proj)
        # cos(chi), sin(chi)
        cos_chi = np.dot(l_proj, e_proj)
        sin_chi = np.dot(e_proj, np.cross(n_los, l_proj))
        print("1 = ", np.hypot(sin_chi, cos_chi))
        return sin_chi, cos_chi

    def _chi(self, point, n_los):
        """
        Position of EVPA in the observer frame w/o polarization swing
        correction. Assuming that EVPA in the observer frame is determined by
        e' (electric field unit vector for the EM wave in plasma frame) and
        B' (unit vector of the magnetic field in the plasma frame). Assumes that
        B in jet ctor is in the plasma frame.

        :param n_los:
            Unit vector along ray to the observer.
        """
        # Jet axis
        l = np.array([0, 0, 1])
        # Projection of jet axis on the plane of the sky
        l_proj = l - np.dot(l, n_los)*n_los
        # Make it unit vector
        l_proj = l_proj/np.linalg.norm(l_proj)
        # print("l_proj = ", l_proj)

        b_unit = self.jet.bfield.value(point)
        b_unit = b_unit / np.linalg.norm(b_unit)
        # print("B_unit ", b_unit)
        v = self.jet.vfield.value(point)
        # print("V ", v)
        n_los_primed = get_n_los_prime(n_los, v)
        # print("N_los_primed ", n_los_primed)
        e_primed = np.cross(n_los_primed, b_unit)

        # Projection of e_primed on a plane perpendicular to n_los
        e_proj = e_primed - np.dot(e_primed, n_los)*n_los
        # Make it unit vector
        e_proj = e_proj/np.linalg.norm(e_proj)
        # cos(chi), sin(chi)
        cos_chi = np.dot(l_proj, e_proj)
        sin_chi = np.dot(e_proj, np.cross(n_los, l_proj))
        print("1 = ", np.hypot(sin_chi, cos_chi))
        return sin_chi, cos_chi

    def integrate_full(self, list_intersect, ray_direction, nu):
        ray_direction_ = -ray_direction

        # def dIQUV_dt(t, y):
        #     # print("t, y ", t, y)
        #     iquv = np.zeros(4, dtype=float)
        #     args = [point+t*ray_direction_, ray_direction_, nu]
        #     # dI/dl
        #     iquv[0] = self.jet.getEtaI(*args)-self.jet.getKI(*args)*y[0]-self.jet.getKQ(*args)*y[1]-self.jet.getKU(*args)*y[2]-self.jet.getKV(*args)*y[3]
        #     # dQ/dl
        #     iquv[1] = self.jet.getEtaQ(*args)-self.jet.getKI(*args)*y[1]-self.jet.getKQ(*args)*y[0]-self.jet.getKF(*args)*y[2]-self.jet.getHQ(*args)*y[3]
        #     # dU/dl
        #     iquv[2] = self.jet.getEtaU(*args)-self.jet.getKI(*args)*y[2]-self.jet.getKU(*args)*y[0]+self.jet.getKF(*args)*y[1]-self.jet.getKC(*args)*y[3]
        #     # dV/dl
        #     iquv[3] = self.jet.getEtaV(*args)-self.jet.getKI(*args)*y[3]-self.jet.getKV(*args)*y[0]+self.jet.getHQ(*args)*y[1]+self.jet.getKC(*args)*y[2]
        #     return iquv

        # No V, no Conv., no Rot.
        def dIQUV_dt(t, y):
            # print("t, y ", t, y)
            iquv = np.zeros(4, dtype=float)
            args = [point+t*ray_direction_, ray_direction_, nu]
            # dI/dl
            iquv[0] = self.jet.getEtaI(*args)-self.jet.getKI(*args)*y[0]-self.jet.getKQ(*args)*y[1]-self.jet.getKU(*args)*y[2]
            # dQ/dl
            iquv[1] = self.jet.getEtaQ(*args)-self.jet.getKI(*args)*y[1]-self.jet.getKQ(*args)*y[0]
            # dU/dl
            iquv[2] = self.jet.getEtaU(*args)-self.jet.getKI(*args)*y[2]-self.jet.getKU(*args)*y[0]
            # dV/dl
            iquv[3] = 0
            return iquv

        # def dIQUV_dt(t, y):
        #     # print("t, y ", t, y)
        #     iquv = np.zeros(4, dtype=float)
        #     args = [point+t*ray_direction_, ray_direction_, nu]
        #     # dI/dl
        #     iquv[0] = self.jet.getEtaI(*args)-self.jet.getKI(*args)*y[0]-self.jet.getKQ(*args)*y[1]-self.jet.getKU(*args)*y[2]-self.jet.getKV(*args)*y[3]
        #     # dQ/dl
        #     iquv[1] = self.jet.getEtaQ(*args)-self.jet.getKI(*args)*y[1]-self.jet.getKQ(*args)*y[0]-self.jet.getKF(*args)*y[2]-self.jet.getHQ(*args)*y[3]
        #     # dU/dl
        #     iquv[2] = self.jet.getEtaU(*args)-self.jet.getKI(*args)*y[2]-self.jet.getKU(*args)*y[0]+self.jet.getKF(*args)*y[1]-self.jet.getKC(*args)*y[3]
        #     # dV/dl
        #     iquv[3] = self.jet.getEtaV(*args)-self.jet.getKI(*args)*y[3]-self.jet.getKV(*args)*y[0]+self.jet.getHQ(*args)*y[1]+self.jet.getKC(*args)*y[2]
        #     return iquv



        IQUV, t = np.zeros(4, dtype=float), 0
        for i, it in enumerate(list_intersect):
            point_in, point_out = it.get_path()
            length = np.linalg.norm(point_in - point_out)
            point = point_in
            rk = RK45(dIQUV_dt, t, IQUV, length, atol=1e-9, rtol=1e-9)
            # FIXME: As RK make rhs calculation during step we need or to
            # subclass it to correct for EVPA swing or make small ``max_step``
            while rk.status == "running":
                rk.step()
                # print("QU = {}".format(rk.y[1:3]))
                # chi_orig = 0.5*np.arctan2(rk.y[2], rk.y[1])*180/np.pi
                # print("Chi original = {}".format(chi_orig))
                P = np.hypot(rk.y[1], rk.y[2])
                chi = 0.5*np.arctan2(rk.y[2], rk.y[1])
                print("Chi direct = {}".format(chi*180/np.pi))
                sin_chi, cos_chi = self._chi(point+rk.t*ray_direction_, ray_direction_)
                print("Sin={}, Cos={}".format(sin_chi, cos_chi))
                cos2chi = cos_chi ** 2 - sin_chi ** 2
                sin2chi = 2 * sin_chi * cos_chi
                print("Cos2Chi(Q) = {}, Sin2Chi(U)={}".format(cos2chi,
                                                              sin2chi))
                # chi = np.arccos(cos_chi)
                # print("Chi direct = ", chi)
                # cos_chi, sin_chi = self.chi(point+rk.t*ray_direction_, ray_direction_)
                # chi = np.arccos(cos_chi)*180/np.pi
                # print("Chi = ", chi)
                # print("CosChi = ", cos_chi)
                # print("SinChi = ", sin_chi)
                print(rk.y)
                rk.y[1] = P*cos2chi
                rk.y[2] = P*sin2chi
                print(rk.y)
            IQUV += rk.y
            t += length
        return IQUV*self.scale

    def get_image(self, value):
        return self.image_plane.image.get_image(value)


if __name__ == "__main__":
    import pandas as pd
    from bfield import (ConstCylinderBField, ConstCylinderBFieldZ, RadialBField,
                        SimulationOutputBField)
    from vfield import (ConstFlatVField, ConstCentralVField,
                        SimulationOutputVField)
    from nfield import (BKNfField, SimulationOutputNField)
    from geometry import (Cylinder, Cone, SimulationOutputGeometry)
    from jet import Jet

    df = pd.read_csv("sim_new.csv.gz", compression="gzip")
    points = df[["r", "r_perp"]].values

    # jet = Jet(ConstCylinderBField(0.1),
    #           BKNfField(5000.0, 1.65),
    #           ConstFlatVField(10),
    #           Cylinder(np.array([0, 0, 1], dtype=float), 0.25))
    #           # Cone(np.array([0, 0, 1], dtype=float), 0.025))

    jet = Jet(SimulationOutputBField(points, df.b_p.values, df.b_phi.values),
              SimulationOutputNField(points, df.n.values),
              SimulationOutputVField(points, df.gamma.values),
              SimulationOutputGeometry(points))

    from image_plane import ImagePlane

    import astropy.units as u
    pixel_size_mas = 10*u.mas
    z = 0.00436
    from astropy import cosmology
    cosmo = cosmology.WMAP9
    # Number of parsecs in one mas
    mas_to_pc = 1/cosmo.arcsec_per_kpc_proper(z).to(u.mas/u.pc)
    pixel_size_pc = (pixel_size_mas*mas_to_pc).value
    print("1 pixel = {} pc".format(pixel_size_pc))

    imgpln = ImagePlane((50, 50), pixel_size_pc, 1.0, np.pi/2)
    nu = 15.35*10**9
    transfer = Transfer(jet, imgpln, nu, z)
    import time
    t0 = time.time()
    transfer.run(lg_tau_max=3, lg_tau_min=-5, mode="I")
    print("time = {}".format(time.time()-t0))

    i = transfer.get_image("I")
    # u = transfer.get_image("U")
    # q = transfer.get_image("Q")
    # v = transfer.get_image("V")
    # chi = np.arctan2(u, q)*0.5
    # mask = i > 0
    # masked_chi = np.ma.array(chi, mask=~mask)
    import matplotlib.pyplot as plt
    plt.matshow(i)
    plt.colorbar()
    plt.show()
