import numpy as np
from abc import ABC, abstractmethod
from scipy.interpolate import LinearNDInterpolator, CloughTocher2DInterpolator
from utils import pc, convert_point_to_r_r_perp


class NField(ABC):
    @abstractmethod
    def value(self, point):
        pass


class SimulationOutputNField(NField):
    def __init__(self, points, values):
        print("Interpolating N-field...")
        self.interpolator = CloughTocher2DInterpolator(points, values,
                                                       fill_value=0.0,
                                                       rescale=True)

    def value(self, point):
        return self.interpolator(convert_point_to_r_r_perp(point))[0]


class BKNfField(NField):
    def __init__(self, n0, n):
        self.n0 = n0
        self.n = n

    def value(self, point):
        return self.n0*np.power(np.linalg.norm(point)/pc, -self.n)


if __name__ == "__main__":
    nfield = BKNfField(1000.0, 2)
    point = np.array([0, 0, 5*pc])
    print("N at given r = {}".format(nfield.value(point)))