import numpy as np
from utils import pc


class Intersection(object):
    def __init__(self, ray, point_in=None, point_out=None):
        self._direction = None
        self._borders = None
        self._is_finite = None
        self.direction = ray.direction
        self.borders = np.array([point_in, point_out])

    @property
    def is_finite(self):
        return self._is_finite

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, direction):
        self._direction = direction

    @property
    def borders(self):
        return self._borders*pc

    def get_path(self):
        return self._borders*pc

    @borders.setter
    def borders(self, borders):
        """
        :param borders:
            List of two vectors.
        """
        self._borders = borders

    def set_point_in(self, point):
        self._borders[0] = point/pc

    def set_point_out(self, point):
        self._borders[1] = point/pc


class FiniteIntersection(Intersection):
    def __init__(self, ray, point_in=None, point_out=None):
        super(FiniteIntersection, self).__init__(ray, point_in, point_out)
        self._is_finite = True


class HalfInfiniteIntersection(Intersection):
    def __init__(self, ray, point, geometry):
        check_point = point - ray.direction
        if geometry.is_within(check_point):
            point_in = point-geometry.big_scale*ray.direction
            point_out = point
        else:
            point_in = point
            point_out = point+geometry.big_scale*ray.direction
        super(HalfInfiniteIntersection, self).__init__(ray, point_in, point_out)
        self._is_finite = False


class InfiniteIntersection(Intersection):
    def __init__(self, ray, geometry):
        diff = ray.origin - geometry.origin
        ref_point = diff - np.dot(diff, ray.direction)*ray.direction
        point_in = ref_point - geometry.big_scale*ray.direction
        point_out = ref_point + geometry.big_scale*ray.direction
        super(InfiniteIntersection, self).__init__(ray, point_in, point_out)
        self._is_finite = False