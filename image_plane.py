import numpy as np
from image import Image
from ray import Ray


class ImagePlane(object):
    def __init__(self, image_size, pixel_size, pixel_scale, los_angle):
        self._image = Image(image_size, pixel_size, pixel_scale)
        self.direction = np.array([-np.sin(los_angle), 0.0, -np.cos(los_angle)])
        self.scale = np.array([1., 1., 1./np.sin(los_angle)])
        self._rays = list()

        for i in range(0, image_size[0]):
            for j in range(0, image_size[1]):
                pixel = self.image.pixels[i*image_size[0]+j]
                coordinate = self.scale*pixel.coordinate
                self._rays.append(Ray(coordinate, self.direction))

    @property
    def pixels(self):
        return self._image.pixels

    @property
    def rays(self):
        return self._rays

    @property
    def image(self):
        return self._image

    @property
    def image_size(self):
        return self._image.image_size