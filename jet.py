import numpy as np
from utils import (getG, getD, get_n_los_prime, k_i, k_i_rnd, eta_i, eta_i_rnd,
                   k_q, k_u, k_v, k_F_r, k_C_r, eta_u, eta_v, h_Q, eta_q, c)


class Jet(object):
    def __init__(self, bfield, nfield, vfield, geometry, gamma_min=100,
                 random_bfield=False):
        self.bfield = bfield
        self.nfield = nfield
        self.vfield = vfield
        self.geometry = geometry
        self.gamma_min = gamma_min
        self.randomB = random_bfield
        self.k_i = k_i if not random_bfield else k_i_rnd
        self.eta_i = eta_i if not random_bfield else eta_i_rnd

    def getV(self, point):
        return self.vfield.value(point)

    def getGamma(self, point):
        return getG(self.vfield.value(point))

    def getB(self, point):
        return self.bfield.value(point)

    def getB_primed(self, point):
        """
        Returns B-field in the plasma frame if it is specified in lab frame.
        """
        gamma = self.getGamma(point)
        b = self.getB(point)
        v = self.getV(point)
        return b/gamma + gamma/((1+gamma)*(c*c))*v*np.dot(v, b)

    def getN(self, point):
        return self.nfield.value(point)

    def getN_primed(self, point):
        """
        Returns N-field in the plasma frame if it is specified in lab frame.
        """
        return self.getN(point)/self.getGamma(point)

    def hit(self, ray):
        return self.geometry.hit(ray)

    def is_within(self, point):
        return self.geometry.is_within(point)

    def getKI(self, point, n_los, nu):
        """
        This is k_i in lab frame that could be integrated along LOS.

        First, comoving frame ``k_i_prime`` (in the rest frame of the emission
        element) is connected to this ``k_i`` as ``k_i = k_i_prime / D``.
        Second, in ``k_i_prime`` we need all quantities in comoving frame
        (primed) in terms of lab frame:

        n_los_prime = f(n_los, v)
        nu_prime = f(nu, n_los, v) = nu/getD

        :param point:
        :param n_los:
        :param nu:
        """
        v = self.vfield.value(point)
        print("v = ", v)
        B = self.getB_primed(point)
        print("B = ", B)
        D = getD(n_los, v)
        n_los_prime = get_n_los_prime(n_los, v)
        nu_prime = nu/D
        N = self.nfield.value(point)
        print("N = ", N)
        return self.k_i(B, n_los_prime, nu_prime, N)/D

    def getKQ(self, point, n_los, nu):
        v = self.vfield.value(point)
        D = getD(n_los, v)
        B = self.getB_primed(point)
        n_los_prime = get_n_los_prime(n_los, v)
        nu_prime = nu/D
        return k_q(B, n_los_prime, nu_prime, self.nfield.value(point))/D

    def getKU(self, point, n_los, nu):
        return 0.0

    def getKV(self, point, n_los, nu):
        v = self.vfield.value(point)
        D = getD(n_los, v)
        B = self.getB_primed(point)
        n_los_prime = get_n_los_prime(n_los, v)
        nu_prime = nu/D
        return k_v(B, n_los_prime, nu_prime, self.nfield.value(point))/D

    def getKF(self, point, n_los, nu):
        v = self.vfield.value(point)
        D = getD(n_los, v)
        B = self.getB_primed(point)
        n_los_prime = get_n_los_prime(n_los, v)
        nu_prime = nu/D
        return k_F_r(B, n_los_prime, nu_prime, self.nfield.value(point), self.gamma_min)/D

    def getKC(self, point, n_los, nu):
        v = self.vfield.value(point)
        D = getD(n_los, v)
        B = self.getB_primed(point)
        n_los_prime = get_n_los_prime(n_los, v)
        nu_prime = nu/D
        return k_C_r(B, n_los_prime, nu_prime, self.nfield.value(point), self.gamma_min)/D

    def getHQ(self, point, n_los, nu):
        return 0.0

    def getEtaI(self, point, n_los, nu):
        """
        This is eta_i in lab frame that could be integrated along LOS.

        First, comoving frame ``eta_i_prime`` (in the rest frame of the emission
        element) is connected to this ``eta_i`` as ``eta_i = D^2 * eta_i_prime``.
        Second, in ``eta_i_prime`` we need all quantities in comoving frame
        (primed) in terms of lab frame:

        n_los_prime = f(n_los, v)
        nu_prime = f(nu, n_los, v) = nu/getD

        :param point:
        :param n_los:
        :param nu:
        :return:
        """
        v = self.vfield.value(point)
        D = getD(n_los, v)
        B = self.getB_primed(point)
        n_los_prime = get_n_los_prime(n_los, v)
        nu_prime = nu/D
        return D*D*self.eta_i(B, n_los_prime, nu_prime, self.nfield.value(point))

    def getEtaQ(self, point, n_los, nu):
        v = self.vfield.value(point)
        D = getD(n_los, v)
        B = self.getB_primed(point)
        n_los_prime = get_n_los_prime(n_los, v)
        nu_prime = nu/D
        return D*D*eta_q(B, n_los_prime, nu_prime, self.nfield.value(point))

    def getEtaU(self, point, n_los, nu):
        return 0.0

    def getEtaV(self, point, n_los, nu):
        v = self.vfield.value(point)
        D = getD(n_los, v)
        B = self.getB_primed(point)
        n_los_prime = get_n_los_prime(n_los, v)
        nu_prime = nu/D
        return D*D*eta_v(B, n_los_prime, nu_prime, self.nfield.value(point))


if __name__ == "__main__":
    from bfield import ConstCylinderBField
    from vfield import ConstFlatVField
    from nfield import BKNfField
    from geometry import Cylinder
    import numpy as np
    from utils import pc, c
    jet = Jet(ConstCylinderBField(1.0, 1),
              BKNfField(1000.0, 2),
              ConstFlatVField(10),
              Cylinder(np.array([0, 0, 1], dtype=float), 1.0))
    point = np.array([0, 0, 5*pc])
    b = jet.bfield.value(point)
    n = jet.nfield.value(point)
    v = jet.vfield.value(point)
    print("B={}, N={}, v={}".format(b,
                                    n,
                                    v / c))
    los_angle = 0.1
    nu = 5*10**9
    n_los = np.array([-np.sin(los_angle), 0.0, -np.cos(los_angle)])
    D = getD(-n_los, v)
    print("D={}".format(D))
    n_los_prime = get_n_los_prime(-n_los, v)
    print("n_los_prime={}".format(n_los_prime))
    print("KI={}".format(jet.getKI(point, -n_los, nu)))

    from utils import k_i, eta_i, nu_b, tgamma
    ki = k_i(b, n_los_prime, nu/D, n, s=2.5)/D
    print("ki = {}".format(ki))
    s = 2.5
    factor = (np.power(3., (s + 1.) / 2.) / 4.) * tgamma(s / 4. + 11. / 6.) * tgamma(s / 4. + 1. / 6.)
    print("factor = {}".format(factor))
    print("nu_b = {}".format(nu_b(b, n_los_prime)))

    etai = eta_i(b, n_los_prime, nu/D, n, s=2.5)*D**2
    print("eta_i = {}".format(etai))