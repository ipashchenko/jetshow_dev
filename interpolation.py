from scipy.interpolate import LinearNDInterpolator, CloughTocher2DInterpolator
import pandas as pd
import numpy as np
import time
from utils import pc

df = pd.read_csv("sim_new.csv.gz", compression="gzip")

import matplotlib.pyplot as plt
dfg = df.groupby("r")
for r, group in dfg:
    plt.plot(group.r_perp[::], group.n[::], '.')
    plt.show()


global_subsample = 1
all_points = df[["r", "r_perp"]].values[::global_subsample]
all_values = df.gamma.values[::global_subsample]

local_subsample = 2
train_points = all_points[::2]
train_values = all_values[::2]
test_points = all_points[1::local_subsample]
test_values = all_values[1::local_subsample]

t0 = time.time()
# interpolator = LinearNDInterpolator(train_points, train_values, fill_value=0.0,
#                                     rescale=True)
interpolator = CloughTocher2DInterpolator(train_points, train_values,
                                          fill_value=0.0,
                                          rescale=True)
print("Interpolator construction for {} points"
      " took {} s".format(len(train_points), time.time() - t0))
t0 = time.time()
pred_values = interpolator(test_points)
print("Interpolation for {} points"
      " took {} s".format(len(test_points), time.time() - t0))
rmse = np.sqrt(np.mean((test_values - pred_values) ** 2))
print("RMSE: ", rmse)

# Finding direction of poloidal B-field
global_subsample = 1
all_points = df[["r", "r_perp"]].values[::global_subsample]
all_values = df.y.values[::global_subsample]
interpolator = CloughTocher2DInterpolator(all_points, all_values,
                                          fill_value=0.0,
                                          rescale=True)
from scipy.optimize import approx_fprime

# ``grad``: first component along ``r``, second - along ``r_perp``.

r2, r_perp2 = np.meshgrid(np.linspace(0, 1400 * pc, 100),
                          np.linspace(0, 3.5 * pc, 100))
rr = np.dstack((r2, r_perp2))
rr = rr.reshape(100 * 100, 2)
grad = np.array([approx_fprime(i, interpolator, 10 ** 13) for i in rr])
grad = grad / np.linalg.norm(grad, axis=0)
grad = np.dot(grad, np.array([[0, -1], [1, 0]]))

plt.quiver(rr[::10, 0] / pc, rr[::10, 1] / pc, grad[::10, 0], grad[::10, 1],
           scale=10, angles="xy")
# plt.axes().set_aspect('equal', 'datalim')
plt.show()


class Interpolator(object):
    _to_interpole = ("b_p", "b_phi", "y")

    def __init__(self, df, global_subsample=1, phi_0=10**33):
        self.df = df
        self.global_subsample = global_subsample
        self.points = df[["r", "r_perp"]].values[::global_subsample]
        self.interpolators = dict()
        self.interpolate()
        self.phi_0 = phi_0

    def interpolate(self):
        for name in self._to_interpole:
            values = df[name].value
            interpolator = CloughTocher2DInterpolator(self.points,
                                                      values[
                                                      ::self.global_subsample],
                                                      fill_value=0.0,
                                                      rescale=True)
            self.interpolators.update({name: interpolator})

    def convert_point_to_r_r_perp(self, point):
        return np.array([point[2], np.hypot(point[0], point[1])])

    def b_p(self, point):
        return self.interpolators["b_p"](self.convert_point_to_r_r_perp(point))

    def b_phi(self, point):
        return self.interpolators["b_phi"](self.convert_point_to_r_r_perp(point))

    def b_cart(self, point):
        b_p = self.b_p(point)
        b_phi = self.b_phi(point)
        # Angle between direction in xy-plane to point and x-axis
        phi = np.atan(point[1], point[0])
        return np.array([-b_phi*np.sin(phi), b_phi*np.cos(phi), b_p])

    def n(self, point):
        return self.interpolators["n"](self.convert_point_to_r_r_perp(point))

    def gamma(self, point):
        return self.interpolators["gamma"](self.convert_point_to_r_r_perp(point))

    def y(self, point):
        return self.interpolators["y"](self.convert_point_to_r_r_perp(point))

    def b_p_unit(self, point, eps=0.01*pc):
        grad = approx_fprime(point, interpolator, eps)
        grad /= np.linalg.norm(grad)
        return np.dot(grad, np.array([[0, -1], [1, 0]]))