import os
import glob
import numpy as np
import pandas as pd
from utils import c, m_p, m_e


# Constants
# Rotation parameter [-]. a = r_g/R_L, the ratio of the gravitational radius to
# the light cylinder radius.
a = 0.13
# Total magnetic flux in a jet [G*cm^2].
Phi_0 = 10**33
# Elena's suggestion
Phi_0 = 60*Phi_0
# Gravitational radius [cm].
r_g = 2*10**15
# Michel's magnetization parameter [-], initial magnetization of a jet.
sigma_m = 100
# Outer pressure parameter [dyn/cm^2].
p_0 = 1.92*10**(-9)
# Distance parameter [cm].
r_0 = 1.8*10**5*r_g


names = ["x", "y", "b_p_nodim", "b_phi_nodim", "n_nodim", "p_nodim",
         "p_jet_nodim", "gamma"]


def b_dim(b_nodim, Phi_0, a, r_g):
    """
    Convert magnetic field from dimensionless units to G.
    """
    return b_nodim*Phi_0*a**2/(2*np.pi*sigma_m*r_g**2)


def r_tr(x, r_g, a):
    """
    Convert ``x`` to perpendicular axis distance [cm].
    """
    return x*r_g/a


def n_dim(n_nodim, Phi_0, a, sigma_m, r_g):
    """
    Convert particle density from dimensionless units to cm^(-3)
    """
    return n_nodim*(Phi_0*a**2/(8*np.pi**2*c*sigma_m*r_g**2))**2/m_e


def p_dim(p_nodim, Phi_0, a, sigma_m, r_g):
    """
    Convert pressure from dimensionless units to dyn/cm^2.
    """
    return p_nodim*(Phi_0*a**2/(2*np.pi*r_g**2*sigma_m))**2


def r_ax(p_jet, p_0, r_0):
    """
    Convert pressure to distance from jet apex [cm].
    """
    return r_0*np.power(p_jet/p_0, -0.4)


def read_single_file_pd(fname, Phi_0, a, r_g, sigma_m, p_0, r_0):
    df = pd.read_table(fname, delim_whitespace=True, names=names,
                       header=None)
    df["r_perp"] = r_tr(df.x, r_g, a)
    p_jet = p_dim(df.p_jet_nodim, Phi_0, a, sigma_m, r_g)
    df["r"] = r_ax(p_jet, p_0, r_0)
    df["b_p"] = b_dim(df.b_p_nodim, Phi_0, a, r_g)
    df["b_phi"] = b_dim(df.b_phi_nodim, Phi_0, a, r_g)
    df["n"] = n_dim(df.n_nodim, Phi_0, a, sigma_m, r_g)
    df = df.drop(["x", "p_nodim", "p_jet_nodim", "b_p_nodim",
                  "b_phi_nodim", "n_nodim"],
                 axis=1)
    cols = df.columns[df.dtypes.eq(object)]
    df[cols] = df[cols].apply(pd.to_numeric, errors='coerce', axis=0)
    return df


def read_all_files(files, Phi_0, a, r_g, sigma_m, p_0, r_0):
    dfs = list()
    for fn in files:
        print("Reading file {}".format(fn))
        dfs.append(read_single_file_pd(fn, Phi_0, a, r_g, sigma_m, p_0, r_0))

    return pd.concat(dfs)


if __name__ == "__main__":
    data_dir = "/home/ilya/Dropbox/Radiation/Data5/"
    files = glob.glob(os.path.join(data_dir, "*.dat"))
    df = read_all_files(files, Phi_0, a, r_g, sigma_m, p_0, r_0)
    # df.to_csv("sim_new.csv.gz", compression="gzip")

    save_dir = "/home/ilya/github/jetshow/cmake-build-debug/"
    subsample = 10
    vfield = df[["r", "r_perp", "gamma"]].values[::subsample]
    np.savetxt(os.path.join(save_dir, "vfield_10_v2.txt"), vfield)
    bfield_p = df[["r", "r_perp", "b_p"]].values[::subsample]
    np.savetxt(os.path.join(save_dir, "bfield_p_10_v2.txt"), bfield_p)
    bfield_fi = df[["r", "r_perp", "b_phi"]].values[::subsample]
    np.savetxt(os.path.join(save_dir, "bfield_fi_10_v2.txt"), bfield_fi)
    nfield = df[["r", "r_perp", "n"]].values[::subsample]
    np.savetxt(os.path.join(save_dir, "nfield_10_v2.txt"), nfield)


