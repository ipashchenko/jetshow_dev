from abc import ABC, abstractmethod
import numpy as np
from astropy import units as u
from intersection import (FiniteIntersection, InfiniteIntersection,
                          HalfInfiniteIntersection)


def intersection(R0, Rd, **kwargs):
    """
    Find intersections of a ray with the following quadric surface:
        F(x, y, z) = Ax^2 + By^2 + Cz^2 + Dxy+ Exz + Fyz + Gx + Hy + Iz + J = 0

    :param R0:
        Reference point of the ray that corresponds to it's affine parameter
        ``t=0``.
    :param Rd:
        Direction of the ray.
    :param kwargs:
        Coefficients of the quadric surface.
    :return:
        Iterable of the affine parameters ``t`` in order of intersections.
    """
    result = list()

    A = kwargs.get("A", 0.0)
    B = kwargs.get("B", 0.0)
    C = kwargs.get("C", 0.0)
    D = kwargs.get("D", 0.0)
    E = kwargs.get("E", 0.0)
    F = kwargs.get("F", 0.0)
    G = kwargs.get("G", 0.0)
    H = kwargs.get("H", 0.0)
    I = kwargs.get("I", 0.0)
    J = kwargs.get("J", 0.0)

    x0 = R0[0]
    y0 = R0[1]
    z0 = R0[2]

    xd = Rd[0]
    yd = Rd[1]
    zd = Rd[2]
    Aq = A*xd**2+B*yd**2+C*zd**2+D*xd*yd+E*xd*zd+F*yd*zd

    Bq = 2*A*x0*xd + 2*B*y0*yd + 2*C*z0*zd + D*(x0*yd + y0*xd) + E*x0*zd +\
         F*(y0*zd + yd*z0) + G*xd + H*yd + I*zd
    Cq = A*x0**2 + B*y0**2 + C*z0**2 + D*x0*y0 + E*x0*z0 + F*y0*z0 + G*x0 +\
         H*y0 + I*z0 + J
    Dscr = Bq**2 - 4*Aq*Cq

    if Aq == 0:
        result.append(-Cq/Bq)
        return result
    elif Dscr < 0:
        result.append(None)
        return result
    else:
        t0 = (-Bq - np.sqrt(Dscr))/(2*Aq)
        if t0 > 0:
            result.append(t0)
        else:
            t1 = (-Bq + np.sqrt(Dscr))/(2*Aq)
            result.extend([t0, t1])
    return result


class Geometry(ABC):
    pc = u.pc.to(u.cm)

    @property
    def origin(self):
        return np.zeros(3, dtype=float)

    @property
    def big_scale(self):
        raise NotImplementedError

    @abstractmethod
    def hit(self, ray):
        """
        :param ray:
            Instance of ``Ray`` object.
        :return:
            List of ``Intersection`` objects.
        """
        pass

    @abstractmethod
    def is_within(self, point):
        pass


class Cylinder(Geometry):
    def __init__(self, direction, r):
        """
        :param direction:
        :param r:
            Radius in pc.
        """
        self._direction = direction
        self.r = r

    @property
    def direction(self):
        return self._direction

    def hit(self, ray):
        ray_origin = ray.origin
        cyl_origin = self.origin
        ray_direction = ray.direction
        cyl_direction = self.direction
        delta = ray_origin-cyl_origin
        v = ray_direction - np.dot(ray_direction, cyl_direction)*cyl_direction
        w = delta - np.dot(delta, cyl_direction)*cyl_direction
        c0 = np.linalg.norm(w)**2-self.r
        c1 = np.dot(v, w)
        c2 = np.linalg.norm(v)**2

        d = c1*c2 - c0*c2

        # No intersections
        if d < 0:
            result = list()

        # Two intersections
        elif d > 0:
            eps = 1 if c1 > 0 else -1
            sqrt_d = np.sqrt(d)
            t1 = (-c1 - eps*sqrt_d)/c2
            t2 = c0/(-c1 - eps*sqrt_d)
            point_in = ray.point(min(t1, t2))
            point_out = ray.point(max(t1, t2))
            result = [FiniteIntersection(ray, point_in, point_out)]

        else:
            # One intersection
            if c2 == 1 and c1 == 0 and c0 == 0:
                t = -c1/c2
                point = ray.point(t)
                result = [FiniteIntersection(ray, point, point)]
            # Along border
            if c2 == 0 and c1 == 0 and c0 == 0:
                result = [InfiniteIntersection(ray, self)]
            # No intersections. Externally along border
            if c2 == 0 and c1 == 0 and c0 > 0:
                result = list()
            # No intersections. Internally along border
            if c2 == 0 and c1 == 0 and c0 <= 0:
                result = [InfiniteIntersection(ray, self)]

        return result

    def is_within(self, point):
        distance = np.linalg.norm(np.cross((point - self.origin), self._direction))
        return distance < self.r

    @property
    def big_scale(self):
        return 100*self.r


class Cone(Geometry):
    def __init__(self, direction, half_open_angle, big_scale_pc=None,
                 origin=None):
        self._direction = direction
        self.angle = half_open_angle
        self._big_scale = big_scale_pc or 100.0
        if origin is None:
            self._origin = np.zeros(3, dtype=float)
        else:
            self._origin = origin

    @property
    def origin(self):
        return self._origin

    @property
    def direction(self):
        return self._direction

    @property
    def big_scale(self):
        return 100

    def is_within(self, point):
        diff = point-self.origin
        diff = diff/np.linalg.norm(diff)
        cosp = np.dot(self.direction, diff)
        return cosp > np.cos(self.angle) or cosp < -np.cos(self.angle)

    def hit(self, ray):
        ray_origin = ray.origin
        ray_direction = ray.direction
        cone_origin = self.origin
        cone_direction = self.direction
        cone_angle = self.angle
        eye_matrix = np.eye(3, 3, dtype=float)

        # DP
        delta = ray_origin - cone_origin
        # M
        M = np.outer(cone_direction, cone_direction.T) - np.cos(cone_angle)**2*eye_matrix

        c2 = ray_direction @ M @ ray_direction
        c1 = ray_direction @ M @ delta
        c0 = delta @ M @ delta
        d = c1*c1 - c0*c2

        if d < 0:
            print("No intersections")
            result = list()
        # One intersection if ray goes through apex of cone or it parallels to
        # any of it's generating lines.
        elif d == 0:
            t = -c1/c2
            point = ray.point(t)
            # One intersection and infinite path before/past
            if np.allclose(point, cone_origin) and c1 == 0:
                result = [InfiniteIntersection(ray, self)]
            # One intersection at apex
            else:
                result = [FiniteIntersection(ray, point, point)]
        else:
            # Infinite intersections only if ray parallel to ray direction
            eps = 1 if c1 > 0 else -1
            t1 = (-c1-eps*np.sqrt(d))/c2
            t2 = c0/(-c1-eps*np.sqrt(d))
            point_in = ray.point(min(t1, t2))
            point_out = ray.point(max(t1, t2))

            cos_ray_cone = np.dot(ray_direction, cone_direction)

            # Two intersection - finite case
            if abs(cos_ray_cone) != 1:
                result = [FiniteIntersection(ray, point_in, point_out)]
            # Two intersection - two half-infinite cases
            else:
                result = [HalfInfiniteIntersection(ray, point_out, self),
                          HalfInfiniteIntersection(ray, point_in, self)]

        return result


class Parabaloid(Geometry):
    def __init__(self, direction, R0, big_scale_pc=None):
        self._direction = direction
        self.R0 = R0
        self._big_scale = big_scale_pc or 100.0

    @property
    def direction(self):
        return self._direction

    @property
    def big_scale(self):
        return 100

    def hit(self, ray):
        result = list()
        ts = intersection(ray.origin, ray.direction, A=1., B=1., I=-self.R0**2)
        if not ts:
            pass
        elif len(ts) == 1:
            # One intersection - ignore ot for now
            pass
        else:
            t1, t2 = ts
            point_in = ray.point(min(t1, t2))
            point_out = ray.point(max(t1, t2))
            result = [FiniteIntersection(ray, point_in, point_out)]
        return result

    def is_within(self, point):
        return np.hypot(point[0], point[1]) < self.R0*np.sqrt(point[2])


class ParabaloidCone(Geometry):
    def __init__(self, direction, R0, z0, big_scale_pc=None,
                 half_open_angle_cone=None):
        """
        Parabaloid that goes into Cone.

        :param R0:
            Radius of parabaloid at z=1.
        :param z0:
            Distance at which parabaloid goes into cone.
        :param half_open_angle_cone: (optional)
            Half-opening angle of the Cone. If ``None`` than it is determined by
            the tangent of the Parabaloid at ``z0``. (default: ``None``)
        """
        self._direction = direction
        self.R0 = R0
        self.z0 = z0
        self._big_scale = big_scale_pc or 100.0

        # Get parameters of the corresponding Cone
        if half_open_angle_cone is None:
            tan_halpha_cone = 0.5*R0*(z0)**(-0.5)
            half_open_angle_cone = np.arctan(tan_halpha_cone)
        else:
            tan_halpha_cone = np.tan(half_open_angle_cone)
        origin_cone = z0*(1./tan_halpha_cone-1.)

        self.cone = Cone(direction, half_open_angle_cone, origin=origin_cone)

    def hit(self, ray):
        result_para = self.hit(ray)
        result_cone = self.cone.hit(ray)
        point_in_para, point_out_para = result_para[0].borders[0]

        if point_in_para[2] > self.z0:
            # Use Cone intersection for point_in
            point_in = result_cone[0].borders[0]
        else:
            point_in = point_in_para

        if point_out_para[2] < self.z0:
            point_out = point_out_para
        else:
            # Use Cone intersection for point_out
            point_out = result_cone[1].borders[1]
        return [FiniteIntersection(ray, point_in, point_out)]

    def is_within(self, point):
        if point[2] > self.z0:
            return self.cone.is_within(point)
        else:
            return self.is_within(point)

class SimulationOutputGeometry(Geometry):
    """
    We can wrap minimum encompassing cylinder to the simulations points and
    create LOS for integration from them. Actually it is quite general approach.
    """
    def __init__(self, points):
        """
        :param points:
            3D numpy array of points coordinates.
        """
        self.point = points
        self._cylinder = self._find_encompassing_cylinder(points)

    def _find_encompassing_cylinder(self, points):
        r = np.sqrt(np.max(np.hypot(points[:, 0], points[:, 1])))
        return Cylinder(np.array([0., 0., 1.]), r)

    def hit(self, ray):
        return self._cylinder.hit(ray)

    def is_within(self, point):
        return self._cylinder.is_within(point)

    @property
    def big_scale(self):
        return self._cylinder.big_scale()


if __name__ == "__main__":
    from ray import Ray

    # geometry = Cylinder(np.array([0, 0, 1]), 1.0)
    # # Two intersections
    # ray = Ray(np.array([0, 0, 0]), np.array([1, 0, 0]))
    # intersection = geometry.hit(ray)
    # print("2: {}".format(intersection[0].borders))
    # # One intersection
    # ray = Ray(np.array([0, 1, 0]), np.array([1, 0, 0]))
    # intersection = geometry.hit(ray)
    # print("1: {}".format(intersection[0].borders))
    # # No intersections externally
    # ray = Ray(np.array([1, 1, 0]), np.array([0, 0, 1]))
    # intersection = geometry.hit(ray)
    # print("no externally: len = {}".format(len(intersection)))
    # # No intersections internally
    # ray = Ray(np.array([0, 0, 0]), np.array([0, 0, 1]))
    # intersection = geometry.hit(ray)
    # print("no internally: {}".format(intersection[0].borders))

    geometry = Cone(np.array([0, 0, 1]), np.pi/4)
    # Two intersections
    ray = Ray(np.array([0, 1, 1]), np.array([0, 1, 0]))
    intersection = geometry.hit(ray)
    print("2: {}".format(intersection[0].borders))
    # # One intersection at apex
    ray = Ray(np.array([0, 1, 0]), np.array([0, 1, 0]))
    intersection = geometry.hit(ray)
    print("1: {}".format(intersection[0].borders))
    # # No intersections externally
    ray = Ray(np.array([2, 2, 0]), np.array([0, 1, 0]))
    intersection = geometry.hit(ray)
    print("no externally: len = {}".format(len(intersection)))
    # Two half-infinite
    ray = Ray(np.array([1, 1, 0]), np.array([0, 0, 1]))
    intersections = geometry.hit(ray)
    print(intersections)
    print("two half-infinite: {}, {}".format(intersections[0].borders,
                                             intersections[1].borders))
