import numpy as np
from abc import ABC, abstractmethod
from scipy.interpolate import LinearNDInterpolator, CloughTocher2DInterpolator
from mayavi.mlab import flow
from utils import pc, convert_point_to_r_r_perp


class BField(ABC):
    @abstractmethod
    def value(self, point):
        pass

    def plot(self, ranges=None):
        if hasattr(self, "interpolator"):
            if ranges is None:
                ranges = [(np.min(self._points[:, i]),
                           np.max(self._points[:, i])) for i in range(3)]
            x, y, z = [np.linspace(_[0], _[1], 100) for _ in ranges]
            xv, yv, zv = np.meshgrid(x, y, z)
            obj = flow(xv, yv, zv, self.interpolator(xv, yv, zv),
                       seedtype="line")
            return obj
        else:
            x, y, z = [np.linspace(_[0], _[1], 100) for _ in ranges]
            xv, yv, zv = np.meshgrid(x, y, z)
            raise NotImplementedError


class SimulationOutputBField(BField):
    def __init__(self, points, b_p_values, b_phi_values):
        print("Inerpolating B-field components...")
        self.interpolator_p = CloughTocher2DInterpolator(points, b_p_values,
                                                         fill_value=0.0,
                                                         rescale=True)
        self.interpolator_phi = CloughTocher2DInterpolator(points, b_phi_values,
                                                           fill_value=0.0,
                                                           rescale=True)

    def b_p(self, point):
        return self.interpolator_p(convert_point_to_r_r_perp(point))

    def b_phi(self, point):
        return self.interpolator_phi(convert_point_to_r_r_perp(point))

    def value(self, point):
        b_p = self.b_p(point)[0]
        print("B_p = ", b_p)
        b_phi = self.b_phi(point)[0]
        print("B_phi = ", b_phi)
        # Angle between direction in xy-plane to point and x-axis
        phi = np.arctan(point[1]/point[0])
        print("phi = ", phi)
        return np.array([-b_phi*np.sin(phi), b_phi*np.cos(phi), b_p])


class ConstCylinderBField(BField):
    def __init__(self, b0):
        self.b0 = b0

    def value(self, point):
        return np.array([0, 0, self.b0])


class ConstCylinderBFieldZ(BField):
    def __init__(self, b0, n):
        self.b0 = b0
        self.n = n

    def value(self, point):
        return np.array([0, 0, self.b0*np.power(np.abs(point[2])/pc, -self.n)])


class ConstCylinderBFieldR(ConstCylinderBFieldZ):
    def value(self, point):
        return np.array([0, 0, self.b0*np.power(np.linalg.norm(point)/pc, -self.n)])


class ToroidalBField(BField):
    def __init__(self, b0, n):
        self.b0 = b0
        self.n = n

    def value(self, point):
        phi = np.arctan(point[1]/point[0])
        b = self.b0*np.power(point[2]/pc, -self.n)
        return np.array([-np.sin(phi)*b, np.cos(phi)*b, 0])


class HelicalCylinderBField(BField):
    def __init__(self, b0, pitch_angle):
        self.b0 = b0
        self.pitch_angle = pitch_angle

    def value(self, point):
        r = np.hypot(point[0], point[1])
        return np.array([self.b0*np.tan(self.pitch_angle*point[1]/r),
                         -self.b0*np.tan(self.pitch_angle*point[0]/r),
                         self.b0])


class RadialBField(BField):
    def __init__(self, b0, n):
        self.b0 = b0
        self.n = n

    def value(self, point):
        r = np.linalg.norm(point)
        return np.array([self.b0*np.power(r/pc, -self.n)*point[0]/r,
                         self.b0*np.power(r/pc, -self.n)*point[1]/r,
                         self.b0*np.power(r/pc, -self.n)*point[2]/r])


class SpiralConicalBField(BField):
    def __init__(self, b0, pitch_angle):
        self.b0 = b0
        self.pitch_angle = pitch_angle

    def value(self, point):
        b_z = self.b0/(point[2]**2/pc**2)
        return np.array([b_z*(point[0]/point[2]+point[1]*np.tan(self.pitch_angle)/pc),
                         b_z*(point[1]/point[2]-point[0]*np.tan(self.pitch_angle)/pc),
                         b_z])


if __name__ == "__main__":
    import pandas as pd
    df = pd.read_csv("sim_new.csv.gz", compression="gzip")
    points = df[["r", "r_perp"]].values
    print(points)
    bfield = SimulationOutputBField(points, df.b_p.values, df.b_phi.values)
    point = np.array([0.1*pc, 0.1*pc, 50*pc])
    print("B at given r = {}".format(bfield.value(point)))

    # bfield = ConstCylinderBField(1.0, 2)
    # point = np.array([0, 0, 5*pc])
    # print("B at given r = {}".format(bfield.value(point)))